import React, { Component } from "react";

class Formulario extends Component {
    constructor() {
        super();
        this.state = {
            username: '',
            name: '',
            languaje: '',
            gender: '',
            about: '',
            message: ''
        }
    };
    save = () => {
        this.setState({
            message: 'guardado'
        })
    };
    handleInput = (e) => {
        if (e.target.name === 'acept') {
            this.setState({
                [e.target.name]: e.target.checked
            })

        } else {

            const { name, value } = e.target;
            this.setState({
                [name]: value
            })
        }


    }

    render() {
        return (
            <div>
                <div>
                    <label htmlFor='username'>Nombre de usuario</label>
                    <input type='text' name='username' onChange={this.handleInput} id='username' value={this.state.username} />
                </div>

                <div>
                    <label htmlFor='name'>Nombre de usuario</label>
                    <input type='text' name='name' id='name' value={this.state.name} onChange={this.handleInput} />
                </div>
                <div>
                    <label htmlFor='languaje'>Idioma</label>
                    <select id='languaje' name='languaje' value={this.state.languaje} onChange={this.handleInput}>
                        <option value=''>selecione un valor </option>
                        <option value='en'>Ingles</option>
                        <option value='es'>Español</option>
                        <option value='de'>Aleman</option>

                    </select>
                </div>
                <div>
                    <label htmlFor='gender'>genero</label>
                    <br />
                    <input type='radio' name='gender' value='m' onChange={this.handleInput} />Hombre
                    <input type='radio' name='gender' value='w' onChange={this.handleInput} />Mujer

                </div>
                <div>
                    <input type='checkbox' id='acept' value={this.state.acept} name='acept' id='acept' onChange={this.handleInput} />Aceptas terminos y condiciones
                </div>
                <div>
                    <label htmlFor='about'>Cuentame algo de ti</label>
                    <textarea value={this.state.about} name='about' onChange={this.handleInput} />

                </div>
                <div>
                    <span style={{ color: 'green' }}>{this.state.message}</span>
                    <button onClick={this.save} >SAVE</button>
                </div>
                <p>{JSON.stringify(this.state)}</p>
            </div>
        )
    }
};
export default Formulario;
