import React, { Component } from 'react';

class UserApi extends Component {
    constructor() {
        super();
        this.state = {
            users: [],
            comments: []
        }
    }

    componentDidMount() {
        fetch('https://jsonplaceholder.typicode.com/users/')
            .then(res => res.json())
            .then(users => this.setState({ users }))
    }


    render() {


        return (
            <div>
                <h2> Cantidad de usuarios</h2>
                <div>
                    <ul>
                        {
                            this.state.users.map(user => {
                                return (
                                    <div key={user.id}>
                                        <li> {user.name} </li>
                                        <p> {user.body} </p>
                                    </div>
                                )
                            })
                        }

                    </ul>
                </div>



            </div>
        )
    }
};
export default UserApi;