import React from 'react';
import Formulario from './components/Formulario';
import UserApi from './components/UserApi';


import './App.css';

function App() {
  return (
    <div className="App">
      <Formulario />
      <UserApi />

    </div>
  );
}

export default App;
